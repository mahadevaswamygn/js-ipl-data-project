const noOfMatchesWonByPerTeam=(matchesData) =>{
    const matchesWonByTeamPerSeason = new Map();
    for (let matchIndex = 0; matchIndex < matchesData.length; matchIndex++) {
        if (matchesData[matchIndex].winner === '' || matchesData[matchIndex].winner === undefined) {
            continue;
        }
        if (matchesWonByTeamPerSeason.has(matchesData[matchIndex].season)) {
            var matchesWonByTeam = matchesWonByTeamPerSeason.get(matchesData[matchIndex].season);
            if (matchesWonByTeam.has(matchesData[matchIndex].winner)) {
                const matchWon = matchesWonByTeam.get(matchesData[matchIndex].winner)
                matchesWonByTeam.set(matchesData[matchIndex].winner, matchWon + 1);
            } else {
                matchesWonByTeam.set(matchesData[matchIndex].winner, 1);
            }
        } else {
            const matchesWonByPerTeam = new Map();
            matchesWonByPerTeam.set(matchesData[matchIndex].winner, 1);
            matchesWonByTeamPerSeason.set(matchesData[matchIndex].season, matchesWonByPerTeam);
        }
    }
    return matchesWonByTeamPerSeason;
};

module.exports=noOfMatchesWonByPerTeam;