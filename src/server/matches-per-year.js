
const noOfMatchesPlayedPerSeason=(matchesData) =>{ 
    const matchesPlayedPerSeason = new Map();
    for (let matchIndex = 0; matchIndex < matchesData.length; matchIndex++) {
        if (matchIndex === 0 || matchIndex === matchesData.length - 1) {
            continue;
        }
        if (matchesPlayedPerSeason.has(matchesData[matchIndex].season)) {
            const matchesPlayed = matchesPlayedPerSeason.get(matchesData[matchIndex].season);
            matchesPlayedPerSeason.set(matchesData[matchIndex].season, matchesPlayed + 1);
        } else {
            matchesPlayedPerSeason.set(matchesData[matchIndex].season, 1);
        }
    }
    return matchesPlayedPerSeason;
};


module.exports=noOfMatchesPlayedPerSeason;