const noOfExtraRunsGivenByTeamsIn2016= (matchesData, deliveriesData)=> {
    const extraRunsGivenByTeamsIn2016 = new Map();
    for (let matchIndex = 0; matchIndex < matchesData.length; matchIndex++) {
        if (matchesData[matchIndex].season === '2016') {
            for (let deliveryIndex = 0; deliveryIndex < deliveriesData.length; deliveryIndex++) {
                if (deliveriesData[deliveryIndex].match_id === matchesData[matchIndex].id) {
                    const extraRuns = deliveriesData[deliveryIndex].extra_runs;
                    if (extraRunsGivenByTeamsIn2016.has(deliveriesData[deliveryIndex].bowling_team)) {
                        const extraRunsByTeam = extraRunsGivenByTeamsIn2016.get(deliveriesData[deliveryIndex].bowling_team);
                        extraRunsGivenByTeamsIn2016.set(deliveriesData[deliveryIndex].bowling_team, parseInt(extraRuns) + parseInt(extraRunsByTeam));
                    }
                    else {
                        extraRunsGivenByTeamsIn2016.set(deliveriesData[deliveryIndex].bowling_team, parseInt(extraRuns));
                    }
                }
            }
        }
    };
    return extraRunsGivenByTeamsIn2016;
}
module.exports=noOfExtraRunsGivenByTeamsIn2016;
