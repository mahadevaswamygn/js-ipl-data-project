const topTenEconomicalBowlersIn2015 =(matchesData, deliveriesData)=> {
    const runsGivenByBowlerIn2015 = new Map();
    const ballsByBowlerIn2015 = new Map();
    const economyOfBowler = new Map();
    const topTenEconomicalBowler = new Map();
    for (let matchIndex = 0; matchIndex < matchesData.length; matchIndex++) {
        if (matchesData[matchIndex].season === '2015') {
            for (let deliveryIndex = 0; deliveryIndex < deliveriesData.length; deliveryIndex++) {
                let totalRuns = deliveriesData[deliveryIndex].total_runs;
                if (deliveriesData[deliveryIndex].match_id === matchesData[matchIndex].id) {
                    if (deliveriesData[deliveryIndex].wide_runs === '0' && deliveriesData[deliveryIndex].penalty_runs === '0' && deliveriesData[deliveryIndex].noball_runs === '0') {
                        if (ballsByBowlerIn2015.has(deliveriesData[deliveryIndex].bowler)) {
                            let balls = ballsByBowlerIn2015.get(deliveriesData[deliveryIndex].bowler);
                            ballsByBowlerIn2015.set(deliveriesData[deliveryIndex].bowler, balls + 1);
                        }
                        else {
                            ballsByBowlerIn2015.set(deliveriesData[deliveryIndex].bowler, 1);
                        }
                    }
                    if (runsGivenByBowlerIn2015.has(deliveriesData[deliveryIndex].bowler)) {
                        let runsByBowler = runsGivenByBowlerIn2015.get(deliveriesData[deliveryIndex].bowler);
                        runsGivenByBowlerIn2015.set(deliveriesData[deliveryIndex].bowler, parseInt(runsByBowler) + parseInt(totalRuns))
                    } else {
                        runsGivenByBowlerIn2015.set(deliveriesData[deliveryIndex].bowler, parseInt(totalRuns))
                    }
                }
            }
        }
    };
    for (const [bowler, runs] of runsGivenByBowlerIn2015) {
        const economy = (runs) / (ballsByBowlerIn2015.get(bowler) / 6);
        economyOfBowler.set(bowler, economy);
    }
    const sortedEconomyOfBowler = Array.from(economyOfBowler).sort((a, b) => { return a[1] - b[1] });
    for (let sortedeconomy = 0; sortedeconomy < 10; sortedeconomy++) {
        topTenEconomicalBowler.set(sortedEconomyOfBowler[sortedeconomy][0], sortedEconomyOfBowler[sortedeconomy][1]);
    }
    return topTenEconomicalBowler;
}
module.exports=topTenEconomicalBowlersIn2015;
