const fs = require('fs');
const calculateMatchesPerYear = require('./server/matches-per-year');
const calculateMatchesWonPerTeamPerYear = require('./server/matches-won-per-team-per-year.js');
const calculateExtraRunsGivenByTeamsInYear = require('./server/no-of-extra-runs-given-by-each-team-in-2016.js');
const calculateTopTenEconomicalBowlersInYear = require('./server/top-ten-economic-bowlers-in-2015.js');

const matchesPerYearJsonFilePath = './public/output/matchesPerYear.json';
const matchesWonByPerTeamPerYearJsonFilePath = './public/output/matchesWonPerTeamPerYear.json';
const extraRunsGivenByTeamsIn2016JSONPath = './public/output/extraRunsGivenByTeamsIn2016.json';
const topTenEconomicalBowlersIn2015JSONPath = './public/output/topTenEconomicalBowlersIn2015.json';

const matches = readMatchesCSVFile();
const deliveries = readDeliveriesCSVFile();

const matchesPerYear = calculateMatchesPerYear(matches);
const matchesWonPerTeamPerYear = calculateMatchesWonPerTeamPerYear(matches);
const extraRunsGivenByTeamsIn2016 = calculateExtraRunsGivenByTeamsInYear(matches, deliveries);
const topTenEconomicalBowlersIn2015 = calculateTopTenEconomicalBowlersInYear(matches, deliveries);

matchesPerYearDataToJSON(matchesPerYear, matchesPerYearJsonFilePath);
matchesWonByperTeamPerYearDataToJSON(matchesWonPerTeamPerYear, matchesWonByPerTeamPerYearJsonFilePath);
extraRunsGivenByTeamsIn2016DataToJSON(extraRunsGivenByTeamsIn2016, extraRunsGivenByTeamsIn2016JSONPath);
topTenEconomicalBowlersIn2015DataToJSON(topTenEconomicalBowlersIn2015, topTenEconomicalBowlersIn2015JSONPath);

function matchesPerYearDataToJSON(data, filePath) {
    const jsonContent = JSON.stringify([...data], null, 2);
    fs.writeFile(filePath, jsonContent, (err) => {
        if (err) {
            console.error('Error writing JSON file:', err);
        }
    });
}

function matchesWonByperTeamPerYearDataToJSON(matchesWonByTeamPerSeason, matchesWonByTeamPerSeasonPath) {
    const plainObject = {};
    for (const [key, nestedMap] of matchesWonByTeamPerSeason) {
        const nestedObject = {};
        if (nestedMap instanceof Map) {
            for (const [nestedKey, nestedValue] of nestedMap) {
                nestedObject[nestedKey] = nestedValue;
            }
        }
        plainObject[key] = nestedObject;
    }
    const jsonContent = JSON.stringify(plainObject, null, 2);
    fs.writeFile(matchesWonByTeamPerSeasonPath, jsonContent, (err) => {
        if (err) {
            console.error('Error writing JSON file:', err);
        }
    });
};

function extraRunsGivenByTeamsIn2016DataToJSON(extraRunsGivenByTeamsIn2016Data, extraRunsGivenByTeamsIn2016JSONPath) {
    const jsonContent = JSON.stringify([...extraRunsGivenByTeamsIn2016Data], null, 2);
    fs.writeFile(extraRunsGivenByTeamsIn2016JSONPath, jsonContent, (err) => {
        if (err) {
            console.error('Error writing JSON file:', err);
        }
    });
}

function topTenEconomicalBowlersIn2015DataToJSON(topTenEconomicalBowlersIn2015Data, topTenEconomicalBowlersIn2015JSONPath) {
    const jsonContent = JSON.stringify([...topTenEconomicalBowlersIn2015Data], null, 2);
    fs.writeFile(topTenEconomicalBowlersIn2015JSONPath, jsonContent, (err) => {
        if (err) {
            console.error('Error writing JSON file:', err);
        }
    });
}

function readMatchesCSVFile() {
    const fs = require('fs');
    const data = fs.readFileSync('/home/mahadeva/Downloads/js-ipl-data-project/src/data/matches.csv', 'utf-8');
    const matchesData = data.split("\n");
    const header = matchesData[0].split(",");
    const matchData = [];

    for (let i = 1; i < matchesData.length; i++) {
        let match = matchesData[i].split(",")
        const match1 = {}

        for (let j = 0; j < header.length; j++) {
            match1[header[j]] = match[j];
        }
        matchData.push(match1);
    }
    return matchData;
}

function readDeliveriesCSVFile() {
    const fs = require('fs');
    const data = fs.readFileSync('/home/mahadeva/Downloads/js-ipl-data-project/src/data/deliveries.csv', 'utf-8');
    const deliveriesData = data.split("\n");
    const header = deliveriesData[0].split(",");
    const deliveryData = [];

    for (let i = 1; i < deliveriesData.length; i++) {
        let delivery = deliveriesData[i].split(",");
        const delivery1 = {}

        for (let j = 0; j < header.length; j++) {
            delivery1[header[j]] = delivery[j];
        }
        deliveryData.push(delivery1);
    }
    return deliveryData;
}




